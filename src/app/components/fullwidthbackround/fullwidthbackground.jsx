import React, { PureComponent } from 'react';

class FullWidthBackground extends PureComponent {
  render() {
    return <div className="full-width-background" />;
  }
}

export { FullWidthBackground };
