import React from 'react';
import PropTypes from 'prop-types';

class Task extends React.PureComponent {
  render() {
    const { onClick, completed, text } = this.props;

    return (
      <li
        onClick={onClick}
        style={{
          textDecoration: completed ? 'line-through' : 'none'
        }}
      >
        {text}
      </li>
    );
  }
}

Task.propTypes = {
  onClick: PropTypes.func.isRequired,
  completed: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired
};

export { Task };
