import React from 'react';
import PropTypes from 'prop-types';
import { Task } from './task';

class TodoList extends React.PureComponent {
  render() {
    const { tasks, toggleTask } = this.props;

    return (
      <ul>
        {tasks.map(task => (
          <Task key={task.id} {...task} onClick={() => toggleTask(task.id)} />
        ))}
      </ul>
    );
  }
}

TodoList.propTypes = {
  tasks: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      completed: PropTypes.bool.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  toggleTask: PropTypes.func.isRequired
};

export { TodoList };
