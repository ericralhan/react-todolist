import { combineReducers } from 'redux';
import tasks from './tasks';
import visibilityFilter from './visibilityfilter';

export default combineReducers({
  tasks,
  visibilityFilter
});
