import React from 'react';

import { Footer } from './components/footer';
import AddTask from './features/addtask';
import VisibleTodoList from './features/visibletodolist';

class App extends React.Component {
  render() {
    return (
      <div>
        <AddTask />
        <VisibleTodoList />
        <Footer />
      </div>
    );
  }
}

export default App;
